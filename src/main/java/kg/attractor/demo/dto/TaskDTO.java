package kg.attractor.demo.dto;

import kg.attractor.demo.model.Task;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.util.UUID;

@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
public class TaskDTO {
    public static TaskDTO from(Task task){
        return builder()
                .id(task.getId())
                .name(task.getName())
                .state(task.getState())
                .description(task.getDescription())
                .taskDate(task.getTaskDate())
                .build();
    }
    private String id;
    private String name;
    private String description;
    private LocalDate taskDate;
    private String state;
}
