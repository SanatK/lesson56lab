package kg.attractor.demo.controller;

import kg.attractor.demo.dto.TaskDTO;
import kg.attractor.demo.model.User;
import kg.attractor.demo.service.TaskService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tasks")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class TaskController {
    TaskService taskService;

    @GetMapping
    public List<TaskDTO> getTasks(Authentication authentication, Pageable pageable){
        return taskService.getTasks(getUserId(authentication), pageable);
    }
    @GetMapping("/{taskId}")
    public Object getUserTask(Authentication authentication, @PathVariable String taskId, Pageable pageable){
        return taskService.getUserTask(getUserId(authentication), taskId, pageable);
    }
    /* форма для создания задачи
     {
        "name":"Wrote to friend",
        "description":"Don't forget text message to Ilya",
        "taskDate":"2020-04-01"
    }
     */
    @PostMapping("/task")
    public TaskDTO createNewTask(Authentication authentication, @RequestBody TaskDTO taskData){
        return taskService.createNewTask(getUserId(authentication), taskData);
    }
    @PutMapping("/task")
    public Object changeTaskState(Authentication authentication, @RequestParam String taskId, Pageable pageable){
        return taskService.changeTaskState(getUserId(authentication), taskId, pageable);
    }

    public String getUserId(Authentication authentication){
        User user = (User) authentication.getPrincipal();
        return user.getId();
    }
}
