package kg.attractor.demo.controller;

import kg.attractor.demo.dto.UserDTO;
import kg.attractor.demo.service.UserService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@RestController
public class UserController {
    UserService userService;
    /* форма для регистрации
     {
 	"id":"u0005",
    "email":"vladi@gmail.com",
    "name":"vladi",
    "password":"0000"
}
     */
    @PostMapping("/register")
    public UserDTO createNewUser(@RequestBody UserDTO userData){
        return userService.createNewUser(userData);
    }
}
