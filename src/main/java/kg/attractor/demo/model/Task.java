package kg.attractor.demo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@Document(collection = "tasks")
public class Task {
    @Id
    @Builder.Default
    private String id = UUID.randomUUID().toString().substring(0,7);
    private String name;
    private String description;
    private LocalDate taskDate;
    private String userId;
    private String state;
}
