package kg.attractor.demo.service;

import kg.attractor.demo.dto.UserDTO;
import kg.attractor.demo.model.User;
import kg.attractor.demo.repository.UserRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Service
public class UserService {
    private final UserRepository userRepo;
    private final PasswordEncoder encoder;

    public UserDTO createNewUser(UserDTO userData){
        var user = User.builder()
                .id(userData.getId())
                .email(userData.getEmail())
                .name(userData.getName())
                .password(encoder.encode(userData.getPassword()))
                .build();
        userRepo.save(user);
        return UserDTO.from(user);
    }

}

