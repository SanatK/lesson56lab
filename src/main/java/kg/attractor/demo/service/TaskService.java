package kg.attractor.demo.service;

import kg.attractor.demo.dto.TaskDTO;
import kg.attractor.demo.exception.ResourceNotFoundException;
import kg.attractor.demo.model.Task;
import kg.attractor.demo.repository.TaskRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class TaskService {
    private final TaskRepository taskRepo;
    public List<TaskDTO> getTasks(String userId, org.springframework.data.domain.Pageable pageable){
        return taskRepo.findAllByUserId(userId, pageable).stream().map(TaskDTO::from).collect(Collectors.toList());
    }
    public Object getUserTask(String userId, String taskId, Pageable pageable){
        List<Task> userTasks = taskRepo.findAllByUserId(userId, pageable);
        for(Task t: userTasks){
            if(t.getId().equals(taskId))
                return TaskDTO.from(t);
        }
        return new ResourceNotFoundException("You don't have task with this id");
    }
    public TaskDTO createNewTask(String userId, TaskDTO taskData){
        var task = Task.builder()
                .name(taskData.getName())
                .description(taskData.getDescription())
                .taskDate(taskData.getTaskDate())
                .userId(userId)
                .state("New Task")
                .build();
        taskRepo.save(task);
        return TaskDTO.from(task);
    }
    public Object changeTaskState(String userId, String taskId, Pageable pageable){
        List<Task> userTasks = taskRepo.findAllByUserId(userId, pageable);
        for(Task t: userTasks){
            if(t.getId().equals(taskId))
                if(t.getState().equals("New Task")){
                    t.setState("In developing");
                    taskRepo.save(t);
                    return TaskDTO.from(t);
                }else if(t.getState().equals("In developing")){
                    t.setState("Completed");
                    taskRepo.save(t);
                    return TaskDTO.from(t);
                }
        }
        return "This task has been completed";
    }
}
