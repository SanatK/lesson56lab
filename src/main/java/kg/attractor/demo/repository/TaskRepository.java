package kg.attractor.demo.repository;

import kg.attractor.demo.model.Task;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface TaskRepository extends PagingAndSortingRepository<Task, String> {
    List<Task> findAllByUserId(String userId, Pageable pageable);
}
