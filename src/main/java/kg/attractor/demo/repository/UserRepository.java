package kg.attractor.demo.repository;

import kg.attractor.demo.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, String> {

    Optional<User> getByEmail(String email);
}
